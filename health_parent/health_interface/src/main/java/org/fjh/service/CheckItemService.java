package org.fjh.service;

import org.fjh.entity.PageResult;
import org.fjh.entity.QueryPageBean;
import org.fjh.pojo.CheckItem;

//服务接口
public interface CheckItemService {
    public void add(CheckItem checkItem);
    public PageResult pageQuery(QueryPageBean queryPageBean);
    public void deleteById(Integer id);
    public void edit(CheckItem checkItem);
    public CheckItem findById(Integer id);
}
