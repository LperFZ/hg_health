package org.fjh.dao;

import com.github.pagehelper.Page;
import org.fjh.pojo.CheckItem;

/**
 * @Author: fengjunhao
 * @Date: 2020/9/27 12:04
 */
public interface CheckItemDao {
	public void add(CheckItem checkItem);
	public Page<CheckItem> selectByCondition(String queryString);
	public long findCountByCheckItemId(Integer id);
	public void deleteById(Integer id);
	public void edit(CheckItem checkItem);
	public CheckItem findById(Integer id);
}
